# headissue public legal documents

## Documents

- [Contributor License Agreement](https://gitlab.com/headissue/legal/blob/master/cla.pdf)
  to clarify the handling of intellectural property
- [General Contractor Agreement](https://gitlab.com/headissue/legal/blob/master/general-contractor-agreement.pdf)
  Dealing in general with  confidentiality and data safety when a person is contracted to provide srevices
- [Privacy Notices to Collaborators](https://gitlab.com/headissue/legal/blob/master/privacy-collaborator.pdf)
  Notice how we store and process data provided, when a person (client, contractor, contributor, etc.) collaborates with us

## Build Prerequisites

Needed packages to render the PDFs:

```
apt install pandoc  texlive-latex-base texlive-fonts-recommended
```
