
PANDOC=pandoc

PANDOC_OPTIONS=--smart --standalone

PANDOC_PDF_OPTIONS=-V papersize:a4

all:	cla.pdf privacy-collaborator.pdf general-contractor-agreement.pdf

%.pdf : %.md Makefile
	$(PANDOC) $(PANDOC_OPTIONS) $(PANDOC_PDF_OPTIONS) -o $@ $<
