# Privacy Notice for Collaborators

_Version 1.0, 5th April 2019_

Notes on privacy and and data processing, in case you are collaborating with us and you provide personal information
that is protected under the General Data Protection Regulation.

## Data Controller

```
headissue GmbH
Streitbergstr. 73a
81249 Munich
Germany
```

## Contact

For questions and inquiries related to privacy, please use the contact form at: https://headissue.com/contact.html

## Your Rights

You have the following rights according to the GDPR Data Subject Rights:

- The right to access their personal information
- The right to request amendments/rectification of their personal data
- The right to request their personal data to be erased, where it is no longer necessary for the
information to be retained
- The right to request a restriction of the processing of their data
- The right to obtain a copy of their personal data (the right to data portability)
- The right to object to the processing of their data
- The right to object to the use of automated decision making, including profiling
- The right to claim compensation for damages caused by an infringement of the Regulation

## Information Stored

We store and process the following types of information provided by you:

**Information for Legal and Contract Fulfillment Purposes:** Contact information, contracts, bank details, invoices, payment records and other accounting data

**Information for Collaboration and Project work:**  Inquiries, CV, skill lists, qualifications and certifications, phone numbers, e-mail address, other contact information you provide, LinkedIn profile link, GitHub profile link, other social media links you provide, information about past work and contracts, work evaluations 

## Storage

The information is stored electronically, secured and backed up according to the GDPR requirements.

## **Retention** 

- Business information for accounting will be held according to local business and tax regulations.
- Contract information will be held according to the individual contract duration.
- Other information will be held up to a maximum of 5 years.

## **Recipients**

- Employees of The Data Controller that fulfill administrative tasks 
- Employees of The Data Controller that have a need to know, e.g. when involved in the same projects, or to fulfill a required business task related to you.
- Subcontractors that have a need to know, e.g. when involved in the same projects, or to fulfill a required business task related to you.

Without your prior written consent we will not disclose your personal data to any third party. 

## **Updates**

This document will be updated to reflect the latest processes and requirements.