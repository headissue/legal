# Contributor License Agreement
 
_Version 1.0 - April 4th 2019_

Additional comments, references and attributions or a later version of this document may be found at: [https://gitlab.com/headissue/legal](https://gitlab.com/headissue/legal)

_This contribution agreement grants us non-exclusive copyright and patent licenses to your contributions. It covers contributions to Open Source projects as well as internal projects. It is based on the Apache Individual Contributor License Agreement._

## Preface

In order to clarify the intellectual property license granted with Contributions from any person or entity, we must have evidence that each Contributor has agreed to the license terms below.

This license is for your protection as a Contributor as well as the protection of the Managing Organization and its users.

## 1. Definitions.

- **"The Organization"** shall mean headissue GmbH.

- **"You"** (or **"Your"**) shall mean the copyright owner or legal entity authorized by the copyright owner that is making this Agreement with the Organization. For legal entities, the entity making a Contribution and all other entities that control, are controlled by, or are under common control with that entity are considered to be a single Contributor. For the purposes of this definition, "control" means (i) the power, direct or indirect, to cause the direction or management of such entity, whether by contract or otherwise, or (ii) ownership of fifty percent (50%) or more of the outstanding shares, or (iii) beneficial ownership of such entity.

- **"Contribution"** shall mean any original work of authorship, including any modifications or additions to an existing work, that is intentionally submitted by You to the Organization for inclusion in, or documentation of, any of the products owned or managed by the Organization (the "Work"). For the purposes of this definition, "submitted" means any form of electronic, verbal, or written communication sent to the Organization or its representatives, including but not limited to communication on electronic mailing lists, source code control systems, and issue tracking systems that are managed by, or on behalf of, the Organization for the purpose of discussing and improving the Work, but excluding communication that is conspicuously marked or otherwise designated in writing by You as "Not a Contribution."

## 2. Grant of Copyright License

Subject to the terms and conditions of this Agreement, You hereby grant to the Organization and to recipients of software distributed by the Organization a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable copyright license to reproduce, prepare derivative works of, publicly display, publicly perform, sublicense, and distribute Your Contributions and such derivative works.

## 3. Grant of Patent License

Subject to the terms and conditions of this Agreement, You hereby grant to the Organization and to recipients of software distributed by the Organization a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable (except as stated in this section) patent license to make, have made, use, offer to sell, sell, import, and otherwise transfer the Work, where such license applies only to those patent claims licensable by You that are necessarily infringed by Your Contribution(s) alone or by combination of Your Contribution(s) with the Work to which such Contribution(s) was submitted. If any entity institutes patent litigation against You or any other entity (including a cross-claim or counterclaim in a lawsuit) alleging that your Contribution, or the Work to which you have contributed, constitutes direct or contributory patent infringement, then any patent licenses granted to that entity under this Agreement for that Contribution or Work shall terminate as of the date such litigation is filed.

## 4. Legally Entitled

You represent that you are legally entitled to grant the above license. If your employer(s) has rights to intellectual property that you create that includes your Contributions, you represent that you have received permission to make Contributions on behalf of that employer, that your employer has waived such rights for your Contributions to the Organization, or that your employer has executed a separate Corporate CLA with the Organization.

## 5. Original Creation

You represent that each of Your Contributions is Your original creation (see section 7 for submissions on behalf of others). You represent that Your Contribution submissions include complete details of any third-party license or other restriction (including, but not limited to, related patents and trademarks) of which you are personally aware and which are associated with any part of Your Contributions.

## 6. Submission of Third-Party Work

Should You wish to submit work that is not Your original creation, You may submit it to the Organization separately from any Contribution, identifying the complete details of its source and of any license or other restriction (including, but not limited to, related patents, trademarks, and license agreements) of which you are personally aware, and conspicuously marking the work as "Submitted on behalf of a third-party: [named here]".

## 7. Open Source Contributions

In case you are contributing to an Open Source project you reserve all right, title, and interest in and to Your Contributions und you may use your own Contributions for any other purpose. This also applies when you are contributing to an internal project, which becomes open sourced at a later time. 

## 8. Notification

You agree to notify the Organization of any facts or circumstances of which you become aware that would make these representations inaccurate in any respect.

## 9. General Provisions

- **(a) Relationships.** Nothing contained in this Agreement shall be deemed to constitute either party a partner, 
   joint venturer or employee of the other party for any purpose.

- **(b) Severability.** If a court finds any provision of this Agreement invalid or unenforceable, the remainder of this Agreement shall be interpreted so as best to effect the intent of the parties.

- **(c) Integration.** This Agreement expresses the complete understanding of the parties with respect to the subject matter and supersedes all prior proposals, agreements, representations, and understandings. This Agreement may not be amended except in a writing signed by both parties.

- **(d) Waiver.** The failure to exercise any right provided in this Agreement shall not be a waiver of prior or subsequent rights.

- **(e) Term.** You accept and agree to the following terms and conditions for Your present and future Contributions submitted to the Organization.

- **(f) Personal Data.** By submitting the contract you are providing personal data to us. We will not make your data available to any third party. Your data is further stored and processed as outline here: [https://gitlab.com/headissue/legal/blob/master/privacy-collaborator.pdf](https://gitlab.com/headissue/legal/blob/master/privacy-collaborator.pdf)

- **(g) Governing Law.** This Agreement shall be governed in accordance with the laws of the Federal Republic of Germany.

- **(h) Jurisdiction.** The parties consent to the exclusive jurisdiction and venue of the federal and state courts located in Munich, Germany in any action arising out of or relating to this Agreement. The parties waive any other venue to which either party might be entitled by domicile or otherwise.

- **(i) Successors & Assigns.** This Agreement shall bind each party’s heirs, successors and assigns. The contractor may not assign or transfer its rights or obligations under this Agreement without the prior written consent of Hiring Party. However, no consent is required f­or an assignment or transfer that occurs: (a) to an entity in which Contractor owns more than fifty percent of the assets; or (b) as part of a transfer of all or substantially all of the assets of Contractor to any party. Any assignment or transfer in violation of this section shall be void.


## 9. Personal Information and Signature

Full name:

___

Nationality:

___

Mailing Address: 

___

Telephone:

___

E-Mail: 

___

Date Signed:

___

Full Signature:

___


To complete the agreement, please complete the following steps:

- Fill in your personal information above
- Please sign each contract page with a short form of date and signature
- Sign at "Full signature"
- Scan the document
- Send an E-Mail with the document as PDF attachment to your contact person

This document is licensed under a 
[Creative Commons Attribution-Share Alike 3.0 Unported License](https://creativecommons.org/licenses/by-sa/3.0/)