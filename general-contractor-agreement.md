# General Contractor Agreement
 
_Incomplete Draft Version_

Additional comments, references and attributions or a later version of this document may be found at: [https://gitlab.com/headissue/legal](https://gitlab.com/headissue/legal)

## Preface

This agreement needs to be singed by any individual contractor providing services and covers the following subject matters:

- Definition of confidential information and the handling of it
- Agreement that we process your personal data
- Handling of personal data of others

## 1. Definitions.

- **"The Organization"** shall mean headissue GmbH, 

- **"You"** (or **"Your"**) shall mean the entity or person providing services for the Organization.

- **"Confidential Information"** shall mean information relating to the Organizations business including but not limited to:
    business and financial records, intellectual property, source code, customer data, proprietary data, security measures, system configuration, 
    design documents. Any other information that, if disclosed, could affect the business of Hiring Party.
    Personal information about users, employees or other contractors.
    Generally information and data which has restricted access, e.g. by means of a authentication.
    Any information that is labeled as "confidential" or "headissue confidential".
    Excluded is any information that: 
    (a) was in Your possession or was known to You, 
    without an obligation to keep it confidential, before such information was disclosed to You by the Organization; 
    (b) is or becomes public knowledge through a source other than You and through no fault of You; 
    (c) is or becomes lawfully available to You from a source other than the Organization; or 
    (d) is disclosed by You with the Organizations prior written approval.

- **Personal Information** Information about an identifiable data subject as defined by the Gernal Data Protection Regulation, see: 
    [https://gdpr-info.eu/issues/personal-data/](https://gdpr-info.eu/issues/personal-data/)   
    For example personal information about users, employees or other contractors.
    
- **Sensitive Information** Data being either Confidential Information and/or Personal Information. 

- **Personal Device** A device You own which is not provided by The Organization, or, a device provided by 
    The Organization which is under Your control.

## 1. Services

Based on separate agreements you provide services including but not limited to: IT infrastructure and systems support, software engineering, consulting, documentation.

## 2. Non-Disclosure of Sensitive Information

Without Hiring Party’s prior written consent, Contractor will not: (a) disclose Confidential Information to any third party; (b) make or permit to be made copies or other reproductions of Confidential Information; or (c) make any commercial use of Confidential Information. The contractor will carefully restrict access to Confidential Information to those of its officers, directors, and employees who are subject to non-disclosure restrictions at least as protective as those set forth in this Agreement and who clearly need such access to participate on Contractor’s behalf to perform Contractor Services.

An individual shall not be held criminally or civilly liable under any federal or state trade secret law for the disclosure of a trade secret that is made (i) in confidence to a federal, state, or local government official, either directly or indirectly, or to an attorney; and (ii) solely for the purpose of reporting or investigating a suspected violation of law; or is made in a complaint or other document filed in a lawsuit or other proceeding, if such filing is made under seal. An individual who files a lawsuit for retaliation by an employer for reporting a suspected violation of law may disclose the trade secret to the attorney of the individual and use the trade secret information in the court proceeding, if the individual (i) files any document containing the trade secret under seal; and (ii) does not disclose the trade secret, except pursuant to court order.

## 4. Return of Materials containing Confidential Information

Upon Hiring Party’s request, you shall immediately return all original materials provided by Hiring Party and any copies, notes or other documents in Contractor’s possession pertaining to Confidential Information.

## 5. Processing of Confidential Information on Personal Devices

Confidential Information may be copied and further processed in Your personal computer. 
Upon finishing an individual contract or upon request these copies and all derived data has to be deleted, including any backups.
Before deletion relevant work results need to be transferred to the Organization.

## 6. Processing of Personal Information on Personal Devices

Data containing Personal Information may not be copied and further processed on Your personal computer.

When collaborating is necessary, the personal data of employees of the Organization or business partners 
is excluded from this rule. In this case the above rule for Confidential Information applies.

## 7. Care and Security Measures

You will employ typical and reasonable care and security measures to protect sensitive information. 
In general included but not limited to:

- Encryption of data on personal devices
- Protection of access to stored data by third parties
- Protection of access to copies of data by third parties
- Physical protection of access to third parties for unencrypted data

The Organization may require additional security measures and may issue regularly updates.   

## 8. Notification of Breach

TODO


## 9. Confidential Information Received 

The Organization does not wish to receive Confidential Information from You. 

## 10. General Provisions

- **(a) Relationships.** Nothing contained in this Agreement shall be deemed to constitute either party a partner, 
   joint venturer or employee of the other party for any purpose.

- **(b) Severability.** If a court finds any provision of this Agreement invalid or unenforceable, the remainder of this Agreement shall be interpreted so as best to effect the intent of the parties.

- **(c) Integration.** This Agreement expresses the complete understanding of the parties with respect to the subject matter and supersedes all prior proposals, agreements, representations, and understandings. This Agreement may not be amended except in a writing signed by both parties.

- **(d) Waiver.** The failure to exercise any right provided in this Agreement shall not be a waiver of prior or subsequent rights.

- **(e) Term.** The non-disclosure terms of this Agreement shall survive any termination, cancellation, expiration or other conclusion of employment (or this Agreement) unless the parties otherwise expressly agree in writing or Provider sends Employee written notice releasing it from this Agreement. 

- **(f) Personal Data.** By submitting the contract you are providing personal data to us. We will not make your data available to any third party. Your data is further stored and processed as outline here: [https://gitlab.com/headissue/legal/blob/master/privacy-collaborator.pdf](https://gitlab.com/headissue/legal/blob/master/privacy-collaborator.pdf)

- **(g) Governing Law.** This Agreement shall be governed in accordance with the laws of the Federal Republic of Germany.

- **(h) Jurisdiction.** The parties consent to the exclusive jurisdiction and venue of the federal and state courts located in Munich, Germany in any action arising out of or relating to this Agreement. The parties waive any other venue to which either party might be entitled by domicile or otherwise.

- **(i) Successors & Assigns.** This Agreement shall bind each party’s heirs, successors and assigns. The contractor may not assign or transfer its rights or obligations under this Agreement without the prior written consent of Hiring Party. However, no consent is required f­or an assignment or transfer that occurs: (a) to an entity in which Contractor owns more than fifty percent of the assets; or (b) as part of a transfer of all or substantially all of the assets of Contractor to any party. Any assignment or transfer in violation of this section shall be void.


## 9. Personal Information and Signature

Full name:

___

Nationality:

___

Mailing Address: 

___

Telephone:

___

E-Mail: 

___

Date Signed:

___

Full Signature:

___


To complete the agreement, please complete the following steps:

- Fill in your personal information above
- Please sign each contract page with a short form of date and signature
- Sign at "Full signature"
- Scan the document
- Send an E-Mail with the document as PDF attachment to your contact person

This document is licensed under a 
[Creative Commons Attribution-Share Alike 3.0 Unported License](https://creativecommons.org/licenses/by-sa/3.0/)
