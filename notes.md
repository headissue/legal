# Notes and References

## CLA - Contributor Agreement

Handles IP of a contributor, which may be a contractor with a business relationship or an independent individual person.

### CLA Resources

Examples of CLAs

- Oracle contributor aggreement: https://www.oracle.com/technetwork/oca-405177.pdf
- Apache has an individual and cooperate CLA: http://apache.org/licenses

### Modifications from the Apache Individual Contributor Agreement:

This contribution agreement is an augmented version of the Apache Individual Contributor License Agreement ("Agreement") V2.0 found at [apache.org/licenses](https://apache.org/licenses), except for the following changes:

- Your agreement is with headissue GmbH, not the Apache Software Foundation, and the term "the Foundation" has been replaced with "the Managing Organization".
- The license preface has been shortened and the requirement for a physical signature has been removed.
- The fields for a physical copy have been moved to the bottom of the page.
- You may opt-out of this agreement for future contributions through an explicit statement, although your contributions will not be accepted.
- This agreement covers contributions to Open Source projects, as well as closed internal projects
- Remove clause "6. Fitness and Support", since there can be additional work contracts with headissue GmbH
- Add "7. Open Source Contribution" to clarify that the contributor retains all rights for its contributions to OS projects
- Add general provisions aka "boilerplate" at the end

## General Contractor Agreement

Should cover a non-mutual NDA, privacy related things.

### NDA Resources

- https://nondisclosureagreement.com
- https://www.docracy.com/0tndg3v8qnj/standard-non-disclosure-agreement-nda

### Is the Contractor a Data Processor?

A contractor is under supervision of the data controller and needs to follow the defined processing rules.

- https://www.linkedin.com/pulse/contractor-processor-john-thompson/

## General stuff

### Comments on the boilerplate / general provisions

- https://nondisclosureagreement.com/independent-contractor.html

### Governing Law / Jurisdiction

- https://internationalcontracts.net/contract/blog/181-governing-law-and-jurisdiction-in-international-contracts
